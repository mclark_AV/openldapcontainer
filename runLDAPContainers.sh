#!/bin/bash

compose=$(docker-compose up -d)
#echo "$compose"
containerSearch=$(docker ps --filter name="ldapcontainer_ldap" --format "table {{.Names}}\t{{.Status}}")

checkRunning=$(echo $containerSearch | grep -c "ldapcontainer_ldap_server_admin_1")

adminURL="http://127.0.0.1:8090"

if [ $checkRunning -gt 0 ]; then
    echo ""
    echo "Waiting 2 seconds before checking containers are up, if you see 2 containers, all is working"
    sleep 2
    echo ""
    result=$(docker ps --filter "name=ldapcontainer_ldap")
    echo "$result"
    echo ""
    echo "Go to this URL to manage: ($adminURL)"
    echo "	Login DN: cn=admin,dc=adaptatest,dc=test"
    echo "	Password: test1234"
    echo ""
    echo "To search the LDAP server on the command line use:"
    echo ""
    echo "	docker exec ldapcontainer_ldap_server_1 ldapsearch -x -H ldap://localhost -b dc=adaptatest,dc=test -D "cn=admin,dc=adaptatest,dc=test" -w test1234"
    echo ""
    echo ""
else
    echo "Containers not running. Check docker logs"
fi

