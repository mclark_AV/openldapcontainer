FROM osixia/openldap

LABEL maintainer="clarkie"

ENV LDAP_ORGANISATION="matts test Test Org" \ 
LDAP_DOMAIN="adaptatest.test"

COPY bootstrap.ldif /container/service/slapd/assets/config/bootstrap/ldif/50-bootstrap.ldif

