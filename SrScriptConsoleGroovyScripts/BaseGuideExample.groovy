import com.onresolve.scriptrunner.ldap.LdapUtil
import org.apache.log4j.Logger
import org.springframework.ldap.core.AttributesMapper
import javax.naming.directory.Attributes
import javax.naming.directory.BasicAttributes
import javax.naming.directory.SearchControls
import org.apache.log4j.Level

def log = Logger.getLogger("LDAP Script")
log.setLevel(Level.DEBUG)

def cnList = LdapUtil.withTemplate('ldap Test Resource') { template ->
    template.search("", "(cn=*)", SearchControls.SUBTREE_SCOPE, { Attributes attributes ->
        attributes.get('cn').get()
    } as AttributesMapper<String>)
}

// cnList will contain all cn's as we searched for cn=*

