
import com.onresolve.scriptrunner.ldap.LdapUtil
import org.apache.log4j.Logger
import org.springframework.ldap.core.AttributesMapper
import javax.naming.directory.Attributes
import javax.naming.directory.SearchControls
import org.apache.log4j.Level

def logger = Logger.getLogger("LDAP Script")
logger.setLevel(Level.DEBUG)

/**
 *
 * @param uid e.g. the user name jsmith
 * @param attributeName e.g. a ldap attribute. e.g: physicaldeliveryofficename, telephonenumber, displayname
 * @return
 */
List<String> findUserAttribute(String uid, String attributeName){

    def attribute = LdapUtil.withTemplate('ldap Test Resource') { template ->
        template.search("", "(uid=$uid)", SearchControls.SUBTREE_SCOPE, { Attributes attributes ->
            logger.debug(attributes)
            attributes.get(attributeName).get()
        } as AttributesMapper<String>)
    }

    return attribute
}

findUserAttribute("jsmith", "physicaldeliveryofficename")
// returns list of Strings for the attribute name searched for e.g:[NewYorkHQ]

/* Example of what a BasicAttributes list looks like

the attributes variable inside the template search closure can be a LinkedList<BasicAttributes> data type
It can contain many of the below objects if the search returns many results
{
initials=initials: JS, givenname=givenName: jsmith, sn=sn: Smith, telephonenumber=telephoneNumber: +1 111 111 1111, userpassword=userPassword: [B@2b37klg8, employeetype=employeeType: full time, departmentnumber=departmentNumber: 1234, physicaldeliveryofficename=physicalDeliveryOfficeName: NewYorkHQ, mail=mail: jsmith@example.com, displayname=displayName: John Smith, objectclass=objectClass: inetOrgPerson, person, organizationalPerson, uid=uid: jsmith, cn=cn: jsmith, title=title: Support, product support, employeenumber=employeeNumber: 15
}

 */