#!/bin/bash

imageCheck=$(docker images | grep -c "codeclark/openldapserver")

if [[ $imageCheck -gt 0 ]] ; then
   echo "You already built this image so you do not need to run this"
   echo "--IMAGE DETAILS:" 
   echo "                $(docker images | grep "codeclark/openldapserver")"
else
   echo "Getting Images, please wait..."
   buildImage=$(docker build -t codeclark/openldapserver:withData .)
   echo "$buildImage"
fi

