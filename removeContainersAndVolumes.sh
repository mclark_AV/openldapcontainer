#!/bin/bash

docker rm -v -f openldapcontainer_ldap_server_1

docker rm -v -f openldapcontainer_ldap_server_admin_1

docker volume rm $(docker volume ls -q -f 'name=openldapcontainer_ldap' -f 'dangling=true')

docker rmi $(docker images codeclark/openldapserver --format "{{.ID}}")