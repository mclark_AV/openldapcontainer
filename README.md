
# Quick Start (Build the Open LDAP Docker container)

 Make sure the bash script files are executable by running: 
	
	chmod +x buildCustomImage.sh
	chmod +x runLDAPContainers.sh
	chmod +x removeContainersAndVolumes.sh
	
CD into the folder where you cloned this repo to. 
i.e. where the `Dockerfile`, `bootstrap.ldif` and `docker-compose.yml` files are located.
 
This directory should also contain the main scripts `./buildCustomImage.sh` and `./runLDAPContainers.sh`

 First run this to build the custom LDAP Container image that contains the data in the bootstrap.ldif file

	./buildCustomImage.sh

 You only need to build the image once, and then again if you delete the image from your machine with docker rmi. If you change the `bootstrap.ldif` file you will need to remove the image. See [Changing the bootstrap ldif file](#Changing-the-bootstrap-ldif-file) header below.

 Then to bring up the LDAP container and also a php LDAP Admin server to manage the LDAP server you run this

	./runLDAPContainers.sh  


# Access the LDAP container through PHP myAdmin

 The `docker-compose.yml` file sets the `admin` user password to `test1234`
 
 It also brings up a `php ldap admin` server which you can use to login to the LDAP server and check it has data.
 
 The URL and credentials to login to the Ldap Server via the php admin server are:
   ```
   url 	    = http://127.0.0.1:8090
   Login/dn    = cn=admin,dc=adaptatest,dc=test
   pass 	    = test1234
   ```
# Run LDAP searches directly via the docker exec command 
 
```
 docker exec <ldapcontainerName> ldapsearch -x -H ldap://localhost -b dc=adaptatest,dc=test -D "cn=admin,dc=adaptatest,dc=test" -w test1234
```

# Using LDAP server with Scriptrunner Resources

Note: You cannot connect to this LDAP server from a docker instance of Jira/Confluence/Bitbucket unless the other containers are on the same docker network. 
Currently, this repo does not connect these LDAP containers to a docker network, so you need to use a local instance of the Atlassian Application using `gradle` or the `atlas-run-standalone` method as documented [here](https://developer.atlassian.com/server/framework/atlassian-sdk/atlas-run-standalone/)

Setup the connection properties like this:
![](images/TestingWithScriptrunner1.png)

Test the connection with a Script to get all CN's
```
import com.onresolve.scriptrunner.ldap.LdapUtil
import org.springframework.ldap.core.AttributesMapper
import javax.naming.directory.Attributes
import javax.naming.directory.SearchControls
import org.apache.log4j.Level

log.setLevel(Level.DEBUG)

def cnList = LdapUtil.withTemplate('ldap Test Resource') { template ->
    template.search("", "(cn=*)", SearchControls.SUBTREE_SCOPE, {Attributes attributes ->
        log.debug(attributes)
        attributes.get('cn').get()
    } as AttributesMapper<String>)
}
```

![](images/TestingWithScriptrunner2.png)


# Credits
• I used this LDAP container as the base: [osixia/openldap](https://hub.docker.com/r/osixia/openldap/tags)

• I used this site [here](https://medium.com/better-programming/ldap-docker-image-with-populated-users-3a5b4d090aa4) to work out the `bootstrap.ldif` file for importing user and group data into LDAP and how to copy it to the new custom image with the Dockerfile, and also how to use the php admin server in the `docker-compose.yml`

• I used this [video](https://www.youtube.com/watch?v=LQjaJINkQXY) to help work out how to build Dockerfiles

# Valid LDIF Properties can be found here

[Valid LDIF Properties](https://www.ietf.org/rfc/rfc2798.txt)
[More Valid LDIP Properties](https://tools.ietf.org/html/rfc4519#page-22)

# Changing the bootstrap ldif file

If you make changes to the bootstrap.ldif after already building and running the container you have to remove the old image, containers and volumes. I made a bash script for this. You can just run this script which will work providing, you have not renamed the default container names which start with `openldapcontainer_ldap` or changed the default image name `codeclark/openldapserver`

```
./removeContainersAndVolumes.sh
```

Then run the `./buildCustomImage.sh` and `runLDAPContainers.sh` in that order again.
If the main `openldapcontainer_ldap_server_1` container does not stay running it is probably because the properties on the `bootstrap.ldif` were invalid.




